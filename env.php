<?php
//定義常數
define('APP_ROOT', dirname(__FILE__));

//引入相關檔案
require_once APP_ROOT . "/config.php";
require_once APP_ROOT . "/connect.php";
require_once APP_ROOT . "/vendor/autoload.php";

use Changken\DB;

//使用DB處理器
$dbHander = new DB($db);