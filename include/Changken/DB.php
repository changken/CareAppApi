<?php
namespace Changken;

class DB
{
    protected $db;

    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
        * executeSql
        * 預執行一條SQL指令,並返回一個PDOStatement物件
        * @param string $sql
        * @param callable|bool $restrict
        * @return \PDOStatement
        */
    private function executeSql($sql, $restrict){
        $result = $this->db->prepare($sql);
        if(is_callable($restrict)){
            $restrict($result);
        }
        $result->execute();
        return $result;
    }

    /**
        * countRows
        * @param string $sql
        * @param callable|bool $restrict
        * @return  int
        */
    public function countRows($sql, $restrict = false)
    {
        $result = $this->executeSql($sql, $restrict);
        return $result->fetchColumn();
    }

    /**
     * getRow
     * @param string $sql.
     * @param callable|bool $restrict
     * @return  \stdClass
     */
    public function getRow($sql, $restrict = false)
    {
        $result = $this->executeSql($sql, $restrict);
        return $result->fetch();
    }

    /**
     * getRowAll
     * @param string $sql.
     * @param callable|bool $restrict
     * @return  array
     */
    public function getRowAll($sql, $restrict = false)
    {
        $result = $this->executeSql($sql, $restrict);
        return $result->fetchAll();
    }

    /**
        * affectRows
        * 主要是給增刪改使用,它會回傳受影響列數!
        * @param string $sql
        * @param callable|bool $restrict
        * @return int
        */
    public function affectRows($sql, $restrict = false)
    {
        $result = $this->executeSql($sql, $restrict);
        return $result->rowCount();
    }
}
