<?php
require_once "env.php";

//如果不是使用POST請求的話
if(!in_array($_SERVER['REQUEST_METHOD'], ["POST"])){
    header("HTTP/1.1 403 Forbidden");
    exit();
}

//檢查請求動作
if(isset($_GET['m'])){
    $action = $_GET['m'];
}else{
    header("HTTP/1.1 403 Forbidden");
    exit();
}

//取得POST資料
$json = file_get_contents('php://input');
$request = json_decode($json, true);

//檢查API_KEY
if($request["api"] != API_KEY){
    header("HTTP/1.1 401 Unauthorized");
    exit();
}

//http header宣告json格式
header('Content-Type: application/json; charset=utf-8');
//header('Content-Type: text/plain; charset=utf-8');

//根據不同的動作來做處理
try{
    switch ($action){
        case "list":
            $response = $dbHander->getRowAll($request["sql"]);
            break;
        case "create":
            $response = [
                "status" => $dbHander->affectRows($request["sql"])
            ];
            break;
        case "retrieve":
            $response = $dbHander->getRow($request["sql"]);
            break;
        case "update":
            $response = [
                "status" => $dbHander->affectRows($request["sql"])
            ];
            break;
        case "delete":
            $response = [
                "status" => $dbHander->affectRows($request["sql"])
            ];
            break;
    }
}catch (PDOException $e){
    //如果prepare有誤
    $response = [
        "status" => $e->getMessage()
    ];
}

//回傳json格式
echo json_encode($response);

$db = null;