<?php
$dsnTpl = "%s:dbhost=%s;dbname=%s;port=%d;charset=%s";

try{
    $db = new PDO(sprintf($dsnTpl, DB_TYPE, DB_HOST, DB_NAME, DB_PORT, DB_CHARSET), DB_USER,DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
}catch (PDOException $e){
    //如果連線錯誤的話
    header('Content-Type: application/json; charset=utf-8');
    $response = [
        "status" => $e->getMessage()
    ];
    echo json_encode($response);
    exit();
}