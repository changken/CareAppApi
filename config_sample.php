<?php
//DATABASE
define('DB_TYPE', 'mysql');
define('DB_HOST', 'HOST HERE');
define('DB_PORT', 3306);
define('DB_USER', 'USER NAME');
define('DB_PW', 'PASSWORD');
define('DB_NAME', 'DB NAME');
define('DB_CHARSET', 'utf8mb4');

//API KEY
define('API_KEY', 'YOUR API KEY');